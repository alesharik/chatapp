package com.alesharik.chatapp.server;

import lombok.RequiredArgsConstructor;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server extends Thread {
    private final ServerSocket socket;
    private final ExecutorService service;
    private final SocketHandler handler;

    public Server(String host, int port, SocketHandler handler) throws IOException {
        socket = new ServerSocket(port, 0, InetAddress.getByName(host));
        service = Executors.newCachedThreadPool();
        this.handler = handler;
        setDaemon(false);
        setName("Server");
    }

    public void shutdown() throws IOException {
        socket.close();
        service.shutdown();
    }

    @Override
    public void run() {
        while (!socket.isClosed()) {
            try {
                Socket accept = socket.accept();
                service.execute(new HandlerImpl(accept));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public interface SocketHandler {
        void handleOpen(Socket socket);

        void handleRead(Socket socket, byte[] data);

        void handleClose(Socket socket);
    }

    @RequiredArgsConstructor
    private final class HandlerImpl implements Runnable {
        private final Socket socket;

        @Override
        public void run() {
            handler.handleOpen(socket);
            try {
                byte[] buf = new byte[4096];
                int nRead;
                while ((nRead = socket.getInputStream().read(buf)) != -1)
                    handler.handleRead(socket, Arrays.copyOf(buf, nRead));
            } catch (IOException ignored) { }
            handler.handleClose(socket);
        }
    }
}
