package com.alesharik.chatapp.server;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        Server server = new Server("0.0.0.0", 1223);

        server.start();
        //noinspection ResultOfMethodCallIgnored
        System.in.read();
        server.shutdown();
    }
}
