plugins {
    java
}

group = "com.alesharik"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    testCompile("junit", "junit", "4.12")
    compile("com.google.code.gson", "gson", "2.8.5")
    compileOnly("org.projectlombok", "lombok", "1.18.2")
}

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_11
}