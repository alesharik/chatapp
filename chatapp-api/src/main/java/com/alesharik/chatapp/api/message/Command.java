package com.alesharik.chatapp.api.message;

import lombok.*;

@RequiredArgsConstructor
@Getter
@EqualsAndHashCode
@ToString
public final class Command<T> {
    private final String type;
    private final T data;
}
