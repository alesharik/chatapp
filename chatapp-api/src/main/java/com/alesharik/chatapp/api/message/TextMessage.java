package com.alesharik.chatapp.api.message;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@RequiredArgsConstructor
@Getter
@ToString
@EqualsAndHashCode
public final class TextMessage {
    private final String from;
    private final String to;
    private final String message;
}
