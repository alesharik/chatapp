package com.alesharik.chatapp.api.message;

import com.google.gson.*;
import lombok.Getter;
import lombok.experimental.UtilityClass;

import java.lang.reflect.Type;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@UtilityClass
public class Gson {
    @Getter
    private static final com.google.gson.Gson GSON = new GsonBuilder()
            .registerTypeAdapter(Command.class, new CommandDeserializer())
            .create();

    private static final Map<String, Class<?>> messages = new ConcurrentHashMap<>();

    static {
        registerMessage("text", TextMessage.class);
    }

    public static void registerMessage(String type, Class<?> clazz) {
        messages.put(type, clazz);
    }

    private static final class CommandDeserializer implements JsonDeserializer<Command<?>> {

        @Override
        public Command<?> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            String type = json.getAsJsonObject().get("type").getAsString();
            Class<?> aClass = messages.get(type);
            if(aClass == null)
                throw new JsonParseException("Type " + type + " not found!");
            return new Command<>(type, context.deserialize(json.getAsJsonObject().get("data").getAsJsonObject(), aClass));
        }
    }
}
