import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    java
    kotlin("jvm") version "1.3.0-rc-80"
}

group = "com.alesharik"
version = "1.0-SNAPSHOT"

repositories {
    maven { setUrl("http://dl.bintray.com/kotlin/kotlin-eap") }
    mavenCentral()
}

dependencies {
    compile(kotlin("stdlib-jdk8"))
    testCompile("junit", "junit", "4.12")
}

subprojects {
    plugins.apply("java")

    dependencies {
        compileOnly("org.projectlombok", "lombok", "1.18.2")
    }
}

allprojects {
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "11"
}