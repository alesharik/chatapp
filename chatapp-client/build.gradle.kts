plugins {
    java
}

group = "com.alesharik"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    testCompile("junit", "junit", "4.12")
    compileOnly("org.projectlombok", "lombok", "1.18.2")
    compile("org.openjfx", "javafx", "11-ea+25", ext = "pom")
    compile(project(":chatapp-api"))
    compileOnly("org.projectlombok", "lombok", "1.18.2")
}

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_11
}